import Vue from 'vue'
import VueRouter from 'vue-router'


import Home from "../components/Home";
import Students from "../components/Students";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
        // redirect: '/students'
    },
    {
        path: '/students',
        name: 'students',
        component: Students
        // name: 'students',
        // component: Students
    },
    // {
    //     path: '/students/:id',
    //     name: 'student',
    //     component: StudentInfo
    // },

]

const router = new VueRouter({
    mode: 'history',
    routes
})


//TODO: hooks and other config

export default router
